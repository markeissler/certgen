## certgen.sh

This is a simple script that leverages openssl to easily generate self-signed server and client
certificates for use when testing TLS-enabled servers and clients in development. **Do not use these certificates in a production environment!** As a convenience, password authentication will be stripped from generated certificates to facilitate testing (entering passwords repeatedly is a drag).


### Usage example for a self-signed client cert:

1 - generate the ca

```
	>certgen.sh ca
```


2 - generate the client cert, specifying the ca

```
    >certgen.sh client ca_key.pem ca_cert.pem
```


3 - generate the server cert, specifying the ca

```
    >certgen.sh server ca_key.pem ca_cert.pem
```


### Adding alternative names for the server certificate
This script supports the x509 v3 Subject Alternative Names (SANs) extension ([RFC 2459] (https://www.rfc-editor.org/rfc/rfc2459.txt)) so that you can generate certificates that match multiple names. Specifically, SANs can contain DNS host names and IP addresses (SANs also support email addresses but this script does not handle that format).

To generate a server certificate with one or more alternative names, first generate the CA certificate as noted above. Then run the command like this:

```
    >echo "IP:0.0.0.0,DNS:test.server.com" | certgen.sh server ca_key.pem ca_cert.pem
```

When verifying the certificate, you should see lines like the following:

```
      X509v3 extensions:
            X509v3 Basic Constraints:
                CA:FALSE
            X509v3 Key Usage:
                Digital Signature, Non Repudiation, Key Encipherment
            X509v3 Subject Alternative Name:
                DNS:test.server.com, IP Address:0.0.0.0
```

Notice how the `X509v3 Subject Alternative Name:` sections includes the specified SANs.

### Verifying certficates

#### generated ca certificate contents:

```
    >openssl x509 -text -noout -in ca_cert.pem
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            a8:e1:01:43:43:76:e1:90
        Signature Algorithm: sha1WithRSAEncryption
        Issuer: C=US, ST=California, L=San Francisco, O=My Company Ltd, CN=test.com/emailAddress=info@test.com
        Validity
            Not Before: Jul  3 01:04:11 2015 GMT
            Not After : Jun 30 01:04:11 2025 GMT
        Subject: C=US, ST=California, L=San Francisco, O=My Company Ltd, CN=test.com/emailAddress=info@test.com
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
            RSA Public Key: (1024 bit)
                Modulus (1024 bit):
                    00:af:6b:30:4d:70:f2:f4:87:6f:58:3a:4f:1f:85:
                    38:11:39:1c:0a:c6:13:d2:cc:c6:56:2f:fe:d0:0b:
                    5e:af:cb:8b:c4:c7:49:b4:f8:f2:22:8a:5b:ab:0f:
                    e2:b4:38:77:d9:23:12:09:bc:c5:53:db:f2:b1:56:
                    33:34:c0:e3:d7:bf:a4:2b:14:10:87:86:f0:8d:65:
                    21:7c:54:5a:03:95:52:7c:9e:2a:43:2a:12:20:87:
                    a8:0e:8a:b9:c9:0c:a6:d2:b7:2c:35:92:0d:fe:67:
                    22:8a:4f:72:30:5a:20:94:ed:23:51:07:48:48:ff:
                    d0:2e:a9:84:de:c8:9a:89:a3
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Basic Constraints:
                CA:TRUE
            X509v3 Subject Key Identifier:
                2F:28:A5:D7:AF:94:F2:6C:D3:66:C2:3A:C7:DA:15:6C:D5:77:80:4F
            X509v3 Authority Key Identifier:
                keyid:2F:28:A5:D7:AF:94:F2:6C:D3:66:C2:3A:C7:DA:15:6C:D5:77:80:4F
                DirName:/C=US/ST=California/L=San Francisco/O=My Company Ltd/CN=test.com/emailAddress=info@test.com
                serial:A8:E1:01:43:43:76:E1:90

    Signature Algorithm: sha1WithRSAEncryption
        44:a4:68:6d:d7:12:51:2c:a0:12:b6:d2:8e:b0:b9:a0:13:ff:
        6a:8a:0b:06:ad:54:36:7c:4e:55:96:d2:66:d4:48:3a:2b:00:
        7d:c5:18:f4:3a:17:62:3d:f8:76:58:82:20:20:b0:0a:6b:6e:
        e5:ac:bf:a2:9e:d1:94:df:32:59:3b:ae:6c:05:ee:59:5c:7c:
        2b:63:84:35:11:ba:1d:e9:cf:81:c3:c3:8d:b6:3d:04:2a:8d:
        52:f7:a5:f5:8f:54:79:89:c9:ed:4e:93:fa:6e:09:53:3c:84:
        f4:f8:35:fa:1f:43:3e:e0:76:c4:aa:61:a3:94:4c:01:a8:66:
        07:b0
```

#### generated client certificate contents:

```
    >openssl x509 -text -noout -in client_cert.pem
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 1 (0x1)
        Signature Algorithm: sha1WithRSAEncryption
        Issuer: C=US, ST=California, L=San Francisco, O=My Company Ltd, CN=test.com/emailAddress=info@test.com
        Validity
            Not Before: Jul  3 01:12:52 2015 GMT
            Not After : Jun 30 01:12:52 2025 GMT
        Subject: C=US, ST=California, L=San Francisco, O=My Company Ltd, CN=test.com/emailAddress=info@test.com
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
            RSA Public Key: (1024 bit)
                Modulus (1024 bit):
                    00:cd:0a:12:b6:41:bd:22:19:1a:a9:65:cb:27:0b:
                    0e:62:f0:75:0d:60:08:75:0e:c9:84:9a:b4:49:08:
                    f5:fd:dd:2c:79:94:36:68:8c:d3:fb:98:99:82:19:
                    56:94:b2:8d:6d:a0:9c:74:e0:44:c6:96:bb:96:f6:
                    7c:b7:40:13:75:35:5e:43:e1:a5:99:03:bd:5a:75:
                    e3:0e:77:8f:d4:17:3f:5e:3c:fc:e9:0e:f3:39:6d:
                    9a:3b:b7:db:a4:d1:8f:36:cd:1f:9e:b5:a2:45:1e:
                    bc:87:10:fc:2c:c2:df:c8:b3:ac:0a:0b:b0:68:63:
                    8d:dd:79:33:70:3f:6e:69:c5
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Basic Constraints:
                CA:FALSE
            X509v3 Key Usage:
                Digital Signature, Non Repudiation, Key Encipherment
    Signature Algorithm: sha1WithRSAEncryption
        af:4f:4c:f1:7f:d0:f1:bd:4a:e8:2b:81:90:91:f8:fc:ab:5d:
        ac:bd:56:bb:a0:87:16:3f:eb:55:ba:06:0b:c0:17:8e:36:99:
        03:c9:fd:0f:84:9a:48:af:a4:c1:4f:52:c0:80:6a:12:77:cb:
        07:21:a9:41:dd:24:2e:89:18:24:18:e6:98:d5:72:c6:2a:8b:
        54:92:f7:83:9c:b4:ee:99:ba:45:24:a8:98:37:c7:3c:e0:86:
        9a:28:6d:b3:ce:00:cc:d7:05:1d:87:de:0d:f9:9d:7a:54:75:
        3b:63:62:9a:f8:ec:65:e7:ab:d8:02:d1:65:09:a6:f9:c8:dd:
        74:1e
```

#### generated server certificate contents:

```
    >openssl x509 -text -noout -in server_cert.pem
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 1 (0x1)
        Signature Algorithm: sha1WithRSAEncryption
        Issuer: C=US, ST=California, L=San Francisco, O=My Company Ltd, CN=test.com/emailAddress=info@test.com
        Validity
            Not Before: Jul  3 01:06:49 2015 GMT
            Not After : Jun 30 01:06:49 2025 GMT
        Subject: C=US, ST=California, L=San Francisco, O=My Company Ltd, CN=test.com/emailAddress=info@test.com
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
            RSA Public Key: (1024 bit)
                Modulus (1024 bit):
                    00:c1:78:ea:40:77:ea:0d:f4:f4:99:c2:d9:ac:56:
                    42:12:f5:c5:7f:e4:cf:e9:7e:0e:95:26:4a:3d:df:
                    2c:71:85:a9:c4:b4:7e:c7:30:d3:db:83:bf:cd:a6:
                    86:b9:a6:b3:8a:59:68:59:ef:bf:60:f9:54:f7:1d:
                    64:7e:42:14:a0:c0:45:5e:9d:ec:2d:f0:9f:b9:be:
                    51:3a:f5:cb:01:82:e2:1b:de:6b:db:20:1b:9a:72:
                    43:3c:82:06:03:f8:98:b0:d3:c4:ce:d5:ee:e6:27:
                    22:0a:7b:ed:97:fa:9c:c9:d2:e1:d5:6b:f8:54:0b:
                    4d:1b:10:6b:51:16:7e:ad:d1
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Basic Constraints:
                CA:FALSE
            X509v3 Key Usage:
                Digital Signature, Non Repudiation, Key Encipherment
            X509v3 Subject Alternative Name:
                IP Address:0.0.0.0
    Signature Algorithm: sha1WithRSAEncryption
        2e:13:66:43:83:64:f5:3f:2c:31:72:8c:eb:31:cf:f3:8f:fd:
        fd:91:c7:b4:66:54:f5:2e:b9:8b:3f:b6:58:fb:4f:bb:c2:98:
        93:07:2a:e7:88:99:ba:db:d9:3a:98:b1:b8:ba:45:a0:60:e8:
        0c:91:88:ba:cc:d0:5a:55:69:96:d4:f7:bd:dc:3b:db:12:6b:
        25:d6:21:cc:1d:78:21:f8:98:a8:30:44:39:62:5a:0b:4e:ff:
        71:38:4f:11:86:0f:fc:51:f2:39:b0:ae:64:cd:a0:09:bb:a6:
        34:b9:a1:ce:9a:bc:31:b3:9d:8a:48:b8:0a:67:cf:a1:bb:60:
        1c:b9
```


## Attributions
The `certain.sh` script was forked from an original script of the same name written by Gong Cheng. This version does not create certificates compatible with Windows XP using xpextensions.

The original work can be found here:

[Generating Self-Signed Test Certificates Using One Single Shell Script](http://www.freesoftwaremagazine.com/articles/generating_self_signed_test_certificates_using_one_single_shell_script)

