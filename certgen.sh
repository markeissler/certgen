#!/usr/bin/env bash
#
# A script generates self-signed root CA cert/key and client/server certs using
# openssl.
#
# This is a modified version of the certgen.sh script by Gong Cheng. This version
# does not create certificates compatible with Windows XP using xpextensions.
# Also, this modified script strips password authentication from the ca cert key
# to facilitate testing (so you don't have to re-enter the password when you
# startup a test server).
#
# Added support for subject alternative names.
#
# Usage example for a self-signed client cert:
#
#   1. generate the ca
#       >certgen.sh ca
#
#   2. generate the client cert, specifying the ca
#       >certgen.sh client ca_key.pem ca_cert.pem
#
#   3. generate the server cert, specifying the ca
#       >certgen.sh server ca_key.pem ca_cert.pem
#
# You can view the generated certificate contents:
#       >openssl x509 -text -noout -in server_cert.pem
#       >openssl x509 -text -noout -in client_cert.pem
#       >openssl x509 -text -noout -in ca_cert.pem
#
# Author: Gong Cheng, chengg11@yahoo.com, Aug. 2008
# Author: Mark Eissler, mark@mixtur.com, Sep. 2013, Jul. 2015
#
# You are free to use this script in any way but absolutely no warranty!
#

usage ()
{
    echo "Usage:"
    echo "  certgen ca [ <ca key> <ca cert> ]"
    echo "  certgen client <ca key> <ca cert> [ <client key>  <client cert> ]"
    echo "  certgen server <ca key> <ca cert> [ <server key>  <server cert> ]"
    echo
    echo "SubjectAltName Support"
    echo "----------------------"
    echo "A comma-separated list of SubjectAltName values can be piped into"
    echo "this script. DNS names must be preceded by the \"DNS:\" label and IP"
    echo "addresses must be preced by the \"IP:\" label. For example..."
    echo
    echo "prompt>echo \"DNS:host.example.com,IP:0.0.0.0,IP:192.168.10.1\" | \\"
    echo "    certgen server ca_key.pem ca_cert.pem"
    echo
    echo "SublectAltName support is only supported for the server option."
    echo
}

gen_config ()
{
    echo "Generating ca_config.cnf"
    cat > ca_config.cnf <<EOT
HOME                    = $ENV::HOME
RANDFILE                = $HOME/.rnd
[ ca ]
default_ca      = CA_default

[ CA_default ]
certs           = .
crl_dir         = .
database        = index.txt
new_certs_dir   = .
certificate     = $2
serial          = serial
private_key     = $1
RANDFILE        = .rand
x509_extensions = usr_cert
name_opt        = ca_default
cert_opt        = ca_default
default_days    = 365
default_crl_days= 30
default_md      = sha1
preserve        = no
policy          = policy_match

[ policy_match ]
countryName             = match
stateOrProvinceName     = match
organizationName        = match
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ policy_anything ]
countryName             = optional
stateOrProvinceName     = optional
localityName            = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ req ]
default_bits            = 1024
default_md              = sha1
default_keyfile         = privkey.pem
distinguished_name      = req_distinguished_name
attributes              = req_attributes
x509_extensions         = v3_ca
req_extensions          = v3_req
string_mask             = MASK:0x2002

[ req_distinguished_name ]
countryName                     = Country Name (2 letter code)
countryName_default             = US
countryName_min                 = 2
countryName_max                 = 2
stateOrProvinceName             = State or Province Name (full name)
stateOrProvinceName_default     = California
localityName                    = Locality Name (eg, city)
localityName_default            = San Francisco
0.organizationName              = Organization Name (eg, company)
0.organizationName_default      = My Company Ltd
organizationalUnitName          = Organizational Unit Name (eg, section)
commonName                      = Common Name (eg, your name or your server's hostname)
commonName_default              = test.com
commonName_max                  = 64
emailAddress                    = Email Address
emailAddress_default            = info@test.com
emailAddress_max                = 64

[ req_attributes ]
challengePassword               = A challenge password
challengePassword_min           = 0
challengePassword_max           = 20
unstructuredName                = An optional company name

[ usr_cert ]
basicConstraints                = CA:FALSE
nsComment                       = "OpenSSL Generated Certificate"
subjectKeyIdentifier            = hash
authorityKeyIdentifier          = keyid,issuer

[ v3_ca ]
basicConstraints                = CA:true
subjectKeyIdentifier            = hash
authorityKeyIdentifier          = keyid:always,issuer:always

[ v3_req ]
basicConstraints                = CA:FALSE
keyUsage                        = nonRepudiation, digitalSignature, keyEncipherment
EOT
}

gen_ca ()
{
    echo "generating CA cert:$1, $2";
    openssl req -config ca_config.cnf -new -x509 -extensions v3_ca -days 3650 -passin pass:whatever -passout pass:whatever -keyout $1 -out $2
    # remove password protection
    openssl rsa -in $1 -out $1 -passin pass:whatever
}

gen_client_cert ()
{
    echo "generating client cert:$1, $2";
    openssl req -config ca_config.cnf -new -nodes -keyout $1 -out temp.csr -days 3650

    openssl ca -config ca_config.cnf -policy policy_anything -extensions v3_req -out $2 -days 3650 -key whatever -infiles temp.csr

    # view: openssl req -text -noout -in temp.csr
    rm temp.csr
}

gen_server_cert ()
{
    echo "generating server cert:$1, $2";
    openssl req -config ca_config.cnf -new -nodes -keyout $1 -out temp.csr -days 3650

    openssl ca -config ca_config.cnf -policy policy_anything -extensions v3_req -out $2 -days 3650 -key whatever -infiles temp.csr

    # view: openssl req -text -noout -in temp.csr
    rm temp.csr
}

check_stdin ()
{
    local input_list=()
    local entry_list=()
    local entry_list_sorted=()
    local dns_count=0 ip_count=0
    local defaultIFS="$IFS"
    local IFS="$defaultIFS"

    # stdin is a tty? nothing has been piped in!
    [[ -t 0 ]] && return

    # config file should have been started already!
    [[ ! -f "ca_config.cnf" ]] && return

    # build a list of all lines that have been piped, this supports both a
    # single line piped on the CLI, along with a multi-line file piped in.
    while IFS= read -r line; do
        line="${line// /}"
        input_list+=( ${line//,/ } )
    done
    # close stdin (pipe)
    exec <&-
    # open /dev/tty as stdin so we can read user prompts if needed
    exec 0</dev/tty

    # piped input only valid for server config
    [[ "${1}" != "server" ]] && return

    [[ ${#input_list} -eq 0 ]] && return

    local i _item _key _val
    for(( i=0; i<${#input_list[@]}; i++ )); do
        _item="${input_list[$i]}";
        # strip spaces
        _item="${_item#// //}"
        # extract key/val
        _key="${_item%:*}"
        _val="${_item##*:}"

        [[ "${_key}" != "DNS" && "${_key}" != "IP" ]] && continue

        if [[ "${_key}" == "DNS" || "${_key}" == "dns" ]]; then
            entry_list+=( "DNS.${dns_count}=${_val}" )
            (( dns_count++ ))
        elif [[ "${_key}" == "IP" || "${_key}" == "ip" ]]; then
            entry_list+=( "IP.${ip_count}=${_val}" )
            (( ip_count++ ))
        fi
    done
    unset i _item _key _val

    [[ ${#entry_list} -eq 0 ]] && return

    # sort the entries array
    IFS=$'\n'
    entry_list_sorted=( $(echo "${entry_list[*]}" | sort ) )
    IFS="$defaultIFS"

    # append to config
    echo "subjectAltName=@alt_names" >> "ca_config.cnf"
    echo >> "ca_config.cnf"
    echo "[ alt_names ]" >> "ca_config.cnf"
    for(( i=0; i<${#entry_list_sorted[@]}; i++ )); do
        echo "${entry_list_sorted[$i]}" >> "ca_config.cnf"
    done
}

#at least one argument
if [ $# -lt 1 ]
then
    usage
    exit 1
fi


#default file names if not specified in command line
ca_key_file=ca_key.pem
ca_cert_file=ca_cert.pem
client_key_file=client_key.pem
client_cert_file=client_cert.pem
server_key_file=server_key.pem
server_cert_file=server_cert.pem

case $1 in
ca)
    if [ $# -eq 3 ]
    then
        ca_key_file=$2
        ca_cert_file=$3
    fi
    gen_config $ca_key_file $ca_cert_file

    # did user pipe in SubjectAltName data?
    check_stdin "$@"

    gen_ca $ca_key_file $ca_cert_file
    if [ -f $ca_key_file -a -f $ca_cert_file ]
    then
        echo "Generated files: key: $ca_key_file , cert: $ca_cert_file"
    else
        echo "Failed to generate all files"
    fi
    ;;
client)
    if [ $# -ne 3 -a $# -ne 5 ]
    then
        usage
        exit 1
    fi
    ca_key_file=$2
    ca_cert_file=$3
    gen_config $ca_key_file $ca_cert_file

    # did user pipe in SubjectAltName data?
    check_stdin "$@"

    if [ x$4 != x ]
    then
        client_key_file=$4
        client_cert_file=$5
    fi
    if [ ! -f index.txt ]
    then
        touch index.txt
    fi

    if [ ! -f serial ]
    then
        echo 01 > serial
    fi
    gen_client_cert $client_key_file $client_cert_file
    if [ -f $client_key_file -a -f $client_cert_file ]
    then
        echo "Generated files: key: $client_key_file , cert: $client_cert_file"
    else
        echo "Failed to generated all files"
    fi
    if [ $client_cert_file != `cat serial.old`.pem ]
    then
        rm `cat serial.old`.pem
    fi
    ;;
server)
    if [ $# -ne 3 -a $# -ne 5 ]
    then
        usage
        exit 1
    fi
    ca_key_file=$2
    ca_cert_file=$3
    gen_config $ca_key_file $ca_cert_file

    # did user pipe in SubjectAltName data?
    check_stdin "$@"

    if [ x$4 != x ]
    then
        server_key_file=$4
        server_cert_file=$5
    fi
    if [ ! -f index.txt ]
    then
        touch index.txt
    fi

    if [ ! -f serial ]
    then
        echo 01 > serial
    fi
    gen_server_cert $server_key_file $server_cert_file
    if [ -f $server_key_file -a -f $server_cert_file ]
    then
        echo "Generated files: key: $server_key_file , cert: $server_cert_file"
    else
        echo "Failed to generated all files"
    fi
    if [ $server_cert_file != `cat serial.old`.pem ]
    then
        rm `cat serial.old`.pem
    fi
    ;;
*) usage; exit 1;;
esac


#cleanups
rm -f ca_config.cnf
rm -f index.txt index.txt.attr index.txt.old
rm -f serial serial.old
